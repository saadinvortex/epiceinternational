<?php
/**
 * Template Name: About Epice
 *
 * Display Testimonials.
 * Display Submit Testimonial form.
 */
get_header();
?>

<div id="whitebg">
    <div id="mainWrapperSingle">
        <div id="mainWrapper">
            <div id="contentMainWrapper">
                <div id="newtop" class="cat">
                    <div class="centerColumn about" id="ezPageDefault">

                        <!-- THIS IS THE 'ABOUT US' PAGE -->
                        <div id="apre" class="col-1-4 blue1 arapey head2">
                            Prevent
                            <div class="circle">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/about-1.png" alt="">
                            </div>
                        </div>
                        <div id="apro" class="col-1-4 blue1 arapey head2">
                            Protect
                            <div class="circle">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/about-2.png" alt="">
                            </div>
                        </div>
                        <div id="aach" class="col-1-4 blue1 arapey head2">
                            Achieve
                            <div class="circle">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/about-3.png" alt="">
                            </div>
                        </div>
                        <div id="amai" class="col-1-4 blue1 arapey head2">
                            Maintain
                            <div class="circle">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/about-4.png" alt="">
                            </div>
                        </div>
                        <br class="clearBoth">
                        <p class="sub1 s">
			<?php
				while ( have_posts() ) : the_post();
					get_template_part( 'content', 'page' );
				endwhile;
			?>  
                        </p>
                        <h3 class="blue1 condensed head1 upp">Our Mission Statement</h3>
                        <?php
                        $the_query = new WP_Query(array('post_type' => 'mission_statement', 'post_status' => 'publish', 'orderby' => 'date', 'order' => 'ASC'));

                        // The Loop
                        if ($the_query->have_posts()) :
                            while ($the_query->have_posts()) : $the_query->the_post();
                                ?>
                                <div class="col-1-3 mission">
                                    <div class="diamond blue1">
                                        <span data-icon="?" aria-hidden="true"><span>
                                            </span></span></div>
                                    <h4 class="upp s sub1"><?php the_title();?></h4>
                                    <p><?php echo the_content();?></p>
                                </div>
                                <?php
                            endwhile;
                        endif;

                        // Reset Post Data
                        wp_reset_postdata();
                        ?>

                        

                      

                        <div id="pipeline">
                            <div class="diamond s">
                                <span data-icon="?" aria-hidden="true"><span>
                                    </span></span></div>
                            <p class="sub1 s"><?php $footNote = get_post_custom_values('foot_note', '5');
                            echo $footNote[0];
                            ?> </p>
                           
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
get_footer();
