<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other 'pages' on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>

<div id="whitebg">
  <div id="mainWrapper">
    <div id="contentMainWrapper">
      <div id="newtop" class="cat">
			<?php
                if ( is_front_page() && twentyfourteen_has_featured_posts() ) {
                    get_template_part( 'featured-content' );
                }
            ?>
			<?php
				while ( have_posts() ) : the_post();
					get_template_part( 'content', 'page' );
				endwhile;
			?>
            
            </div>
         </div>
	</div>
</div>
<script type="text/javascript"> 
    $(document).ready(function() { 
      
      var $container = $('#indexProductList');

      $container.isotope({
        itemSelector : '.productlisting',		
		onLayout: function( elems, instance ) {		
			$('.productlisting:not(.isotope-hidden)').each(function(index){				
				$(this).removeClass (function (i, css) {    
					return (css.match (/\bbox\S+/g) || []).join(' ');
				});					
				$(this).addClass("box"+index);						
				if(index%3!=0)				
				$(this).removeClass("line").addClass("line");				
				else				
				$(this).removeClass("line");				
				//console.log($( window ).width());									
			});				
		}
      });
      
      var $optionSets = $('#refine'),
          $optionLinks = $optionSets.find('a');

      $optionLinks.click(function(){
        var $this = $(this);
        // don't proceed if already selected
        if ( $this.hasClass('selected') ) {
          return false;
        }
        var $optionSet = $this.parents('.option-set');
        $optionSet.find('.selected').removeClass('selected');
        $this.addClass('selected');
  
        // make option object dynamically, i.e. { filter: '.my-filter-class' }
        var options = {},
            key = $optionSet.attr('data-option-key'),
            value = $this.attr('data-option-value');
        // parse 'false' as false boolean
        value = value === 'false' ? false : value;
        options[ key ] = value;		 
        if ( key === 'layoutMode' && typeof changeLayoutMode === 'function' ) {
          // changes in layout modes need extra logic
          changeLayoutMode( $this, options )		  		 
        } else {
          // otherwise, apply new options		  		 
          $container.isotope( options );
        }
        
        return false;
      });

	 });

</script> 

 <script type="text/javascript"> 
        $(document).ready(function() { 
		
			$("#ref").click(function() {
				$("div#ref").toggleClass("active")
				$("#refine").slideToggle("slow");
			}); 
			
			$("#ref,#refine").hover(function() {
				$("div#ref").toggleClass("active")
			}); 
		
			$('#ref,#refine').hover(function() {
				$('#refine').stop().fadeIn();
			}, function(){
				$('#refine').stop().fadeOut();
			});
			
			$('.platbb').click(function(){
				$(this).addClass("clicked");
			});
			
            $('.platb').ajaxForm(function() { 
				$(".clicked").text('Added to Bag!');
				$("#bag").load('ajax-cart-count.html');
            }); 
			
        }); 
</script>
<?php

get_footer();
