<?php
/**
 * Template Name: Media News
 *
 * Display Posts for media and news.
 */
global $wp_query;
?>
<?php get_header(); ?>

<div id="whitebg">
    <div id="mainWrapperSingle">  
        <div id="mainWrapper">
            <div id="contentMainWrapper">
                <div id="newtop" class="cat"> 

                    <!-- bof upload alerts --> 
                    <!-- eof upload alerts -->

                    <div class="centerColumn news" id="ezPageDefault"> 

                        <!-- THIS IS THE 'NEWS' PAGE -->

                        <div id="newsfilter">
                            <form method="post" action="">
                                <input type="text" name="search_data" placeholder="Search here..." class="sub1a upp condensed" />
                            </form>
                            <label class="wrapselect">
                                <select class="main condensed upp sub1a">
                                    <option value="">Month</option>
                                    <option value="*">Show all</option>
                                    <?php
                                    $years = $wpdb->get_results("SELECT distinct DATE_FORMAT(post_date, '%m-%Y') as data FROM $wpdb->posts WHERE post_status = 'publish' AND post_type = 'post' group by post_date ORDER BY post_date DESC");
                                    foreach ($years as $year)
                                    {
                                        ?>
                                        <option value=".f-<?php echo $year->data; ?>"><?php echo date('F Y', strtotime("01-" . $year->data)); ?></option>
                                        <?php
                                    }
                                    wp_reset_query();
                                    wp_reset_postdata();
                                    ?>
                                </select>
                                <span class="sub1a head upp forward condensed">Filter by:</span></label>
                        </div>
                        <br class="clearBoth" />
                        <div id="newswrap" style="overflow: visible !important;">

                            <?php
                            if (isset($_REQUEST['search_data']) && $_REQUEST['search_data'] != '')
                            {
                                $postData = $wpdb->get_results("select * from $wpdb->posts where post_type = 'post' and post_status = 'publish'  and (post_content like '%" . $_REQUEST['search_data'] . "%' OR post_title like '%" . $_REQUEST['search_data'] . "%' OR post_name like '%" . $_REQUEST['search_data'] . "%')");
                                if ($postData):
                                    foreach ($postData as $post)
                                    {
                                        setup_postdata($post);
                                        ?>
                                        <div class="col-1-3 apost f-<?php the_time('m-Y') ?>">
                                            <a href="<?php the_permalink() ?>">
                                                <?php the_post_thumbnail('thumbnail',array('class' => 'smooth')); ?>
                                            </a>
                                            <div class="newsdate"><?php the_time('d/m/Y') ?></div>
                                            <a href="<?php the_permalink() ?>">
                                                <h4 class="condensed blue1 upp head2"> <?php the_title(); ?></h4></a>
                                            <p>
                                               <?php echo substr(get_the_excerpt(),0,50); echo '... ';?>
                                                <a class="newsarrow blue1" href="<?php the_permalink() ?>"></a>
                                            </p>
                                        </div>

                                    <?php
                                    }
                                    wp_reset_query();
                                    wp_reset_postdata();
                                    ?>

                                <?php else: ?>
                            <div id="shippingInfoMainContent"><p><?php echo 'Sorry, no posts matched your criteria.'; ?></p></div>
                                <?php
                                endif;
                            }
                            else
                            {


                                $args = array('post_type=post&post_status=publish&posts_per_page=10');
                                $the_query = new WP_Query($args);
                                if ($the_query->have_posts()):
                                    while ($the_query->have_posts()) : $the_query->the_post();
                                        //setup_postdata( $the_query->the_post());
                                        ?>
                                        <div class="col-1-3 apost f-<?php the_time('m-Y') ?>">
                                            <a href="<?php the_permalink() ?>">
            <?php the_post_thumbnail('thumbnail', array('class' => 'smooth')); ?>
                                            </a>

                                            <div class="newsdate"><?php the_time('d/m/Y') ?></div>
                                            <a href="<?php the_permalink() ?>">
                                                <h4 class="condensed blue1 upp head2"> <?php the_title(); ?></h4></a>
                                            <p>
            <?php echo substr(get_the_excerpt(),0,50); echo '... '; ?>
                                                <a class="newsarrow blue1" href="<?php the_permalink() ?>"></a>
                                            </p>
                                        </div>
                                    <?php endwhile;
                                    wp_reset_postdata(); ?>
                                <?php else: ?>
                                    <p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
                                <?php
                                endif;
                                wp_reset_query();
                            }
                            ?>
                            <div class="navigation">
                                <span class="newer">
<?php previous_posts_link(__('« Newer', 'example')) ?></span>
                                <span class="older"><?php next_posts_link(__('Older »', 'example')) ?></span>
                            </div><!-- /.navigation -->








                        </div>
                        <script type="text/javascript"> 
                            $(document).ready(function() { 
                                $(function() {

                                    var $container = $('#newswrap'),
                                    $select = $('#newsfilter select');

                                    $container.isotope({
                                        itemSelector: '.apost'
                                    });

                                    $select.change(function() {
                                        var filters = $(this).val();
                                        $container.isotope({
                                            filter: filters
                                        });
                                    });

                                });
                            });

                        </script> 
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php get_footer(); ?>
