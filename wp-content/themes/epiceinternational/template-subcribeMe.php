<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other 'pages' on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
//Template Name: Subscribe Me
get_header();
?>

<div id="whitebg">
    <div id="mainWrapper">
        <div id="contentMainWrapper">
            <div id="newtop" class="cat">
                <div class="centerColumn" id="contactUsDefault">
                    <p class="plainBox"></p><p>Thank you for taking a moment to subscribe to our newsletter.</p>
                    <p>Sign up below and we'll send you the latest news, offers and promotions.</p>
                    <p></p>
                    <p>
                    </p>
                    <div id="subscribe" class="smooth">
                        <?php
                        $args = array(
                                'prepend' => '', 
                                'showname' => true,
                                'nametxt' => '', 
                                'nameholder' => 'Your name...', 
                                'emailtxt' => '',
                                'emailholder' => 'Your email address...', 
                                'showsubmit' => true, 
                                'submittxt' => 'Subscribe Now',
                                'jsthanks' => false,
                                'thankyou' => 'Thank you for subscribing to our mailing list'
                                );
                                echo smlsubform($args);
?>
                    </div>	
                    <p></p>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
get_footer();
