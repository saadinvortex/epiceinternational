<?php if(isset($cart_messages) && count($cart_messages) > 0) { ?>
	<?php foreach((array)$cart_messages as $cart_message) { ?>
		<span class="cart_message"><?php echo esc_html( $cart_message ); ?></span>
	<?php } ?>
<?php } ?>

<?php if(wpsc_cart_item_count() > 0): ?>
  <?php while(wpsc_have_cart_items()): wpsc_the_cart_item(); ?>
  <div id="id8" class="item id8">
    <div class="col-1-5"> 
  	  <img src="<?php echo wpsc_cart_item_image(); ?>" alt="<?php echo wpsc_cart_item_name(); ?>" title="<?php echo wpsc_cart_item_name(); ?>" class="product_image" /> 
    </div>
        <div class="product">
              <p> <a href="<?php echo esc_url( wpsc_cart_item_url() ); ?>">
                <?php do_action ( "wpsc_before_cart_widget_item_name" ); ?>
                <?php echo wpsc_cart_item_name(); ?>
                <?php do_action ( "wpsc_after_cart_widget_item_name" ); ?>
                </a>
              </p>
              <?php echo wpsc_cart_item_quantity(); ?> x <?php echo wpsc_cart_item_price(); ?>
              <form action="" method="post" class="adjustform">
                <input type="hidden" name="quantity" value="0" />
                <input type="hidden" name="key" value="<?php echo wpsc_the_cart_item_key(); ?>" />
                <input type="hidden" name="wpsc_update_quantity" value="true" />
                <input class="del deleteItem" type="submit" name="Remove" value="X"/>
              </form>
          </div>
  </div>
  <?php endwhile; ?>
  <div id="bottom">
    <div class="col-1-2">
      <p>
        <?php //printf( _n('%d item', '%d items', wpsc_cart_item_count(), 'wpsc'), wpsc_cart_item_count() ); ?>
        <?php _e('Subtotal:', 'wpsc'); ?>
        <br />
        <?php echo wpsc_cart_total_widget( false, false ,false ); ?> <small>
        <?php //_e( 'excluding discount, shipping and tax', 'wpsc' ); ?>
        </small>
      </p>
    </div>
    <div class="col-1-2"> 
    <a target="_parent" href="<?php echo esc_url( get_option( 'shopping_cart_url' ) ); ?>" title="<?php esc_html_e('Checkout', 'wpsc'); ?>" class="gocheckout">
      <?php esc_html_e('Checkout', 'wpsc'); ?>
      </a>
      <?php /*?> <form action="" method="post" class="wpsc_empty_the_cart">
					<input type="hidden" name="wpsc_ajax_action" value="empty_cart" />
					<a target="_parent" href="<?php echo esc_url( add_query_arg( 'wpsc_ajax_action', 'empty_cart', remove_query_arg( 'ajax' ) ) ); ?>" class="emptycart" title="<?php esc_html_e('Empty Your Cart', 'wpsc'); ?>"><?php esc_html_e('Clear cart', 'wpsc'); ?></a>
		</form>
	<?php */?>
    </div>
  </div>

<!--close shoppingcart-->
<?php else: ?>
<div id="bottom">
		<div class="col-1-2">
			<p>Subtotal:<br>
			<strong class="blue1">$0.00</strong>
			</p>
		</div>
</div>
<?php endif; ?>
<?php wpsc_google_checkout();?>
