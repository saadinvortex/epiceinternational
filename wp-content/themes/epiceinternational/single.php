<?php
/**
 * The Template for displaying all single posts
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>

<div id="whitebg">
  <div id="mainWrapper">
    <div id="contentMainWrapper">
      <div id="newtop" class="cat"> 
        
        <!-- bof upload alerts --> 
        <!-- eof upload alerts -->
        
        <div class="centerColumn newspost" id="ezPageDefault">
          <div id="newsfilter">
            <form method="post" action="/">
              <input type="text" name="q" placeholder="Search here..." class="sub1a upp condensed" />
            </form>
          </div>
          <div id="mainpost" class="col-3-4">
            <?php
			// Start the Loop.
			while ( have_posts() ) : the_post(); ?>
            <h1 class="head1 condensed blue1 upp"> <?php the_title(); ?> </h1>
            <h2 class="bt20">Written on  <?php the_time('d/m/Y') ?> </h2>
            <?php $id = get_post_thumbnail_id($id); ?>
           		 <a href="<?php wp_get_attachment_url($id);?>"> <?php the_post_thumbnail();?></a>
            <div>
              <?php the_content();?>
            </div>
            <?php endwhile;?>
          
            <br class="clearBoth" />
            <div id="addthis">
              <div id="handle" class="sub1a smooth condensed main upp"><span>Share Article</span> <span id="si" class="smooth"></span></div>
              <div id="share">
                <div class="addthis_toolbox addthis_default_style addthis_16x16_style"> 
                	<a class="addthis_button_twitter"><span class="st-twitter"></span></a>
                	 <a class="addthis_button_facebook"><span class="st-facebook"></span></a> 
                	 <a class="addthis_button_google_plusone_share"><span class="st-googleplus"></span></a>
                 	 <a class="addthis_button_pinterest_share"><span class="st-pinterest"></span></a>
                 	  <a class="addthis_button_email"><span class="st-email"></span></a>
                  </div>
              </div>
            </div>
            <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=xa-5294746568c2de07"></script> 
            <br class="clearBoth" />
            <h4 class="condensed blue1 upp head1 hys">Have your say!</h4>
            <div id="tellus" class="smooth oneposttu">
              <?php  if( comments_open() || get_comments_number() ) {


   $fields =  array(
        '<input id="author" name="author" type="text" placeholder="Enter your name here..." value="' . esc_attr( $commenter['comment_author'] ) . '" ' . 'aria-required="true"' . ' required="required" />',
        );
    $comments_args = array(
        'fields' => $fields,
        'comment_field' => '<textarea name="comment" id="comment" placeholder="Comment here..." required="required" ></textarea>');
	$comments_args = array(
        'fields' => $fields,'comment_notes_after' => '<button type="submit" class="pseudo subcom" id="submit-new"><span>'.__('Post').'</span></button>'	);
    comment_form( $comments_args ); ?>
    <?php } ?>
    
 </div>
  <?php
	global $post;
 
$args = array(
	'status' => 'approve',
	'number' => '5',
	'post_id' => $post->ID, // use post_id, not post_ID
);
$comments = get_comments($args);

echo "<ul>";

foreach($comments as $comment) :
	$date=$comment->comment_date;
	echo "<li class='comment comm1'>";
	echo "<strong>".$comment->comment_author."- ".$comment->comment_date."</strong>";
	echo( $comment->comment_content);
	echo "</li>";
endforeach;
echo "</ul>";
?>
            
            
</div>
            
          <?php get_sidebar('content'); ?>
          <div id="prevnext">
            <?php
		 $prev_post = get_adjacent_post(false, '', true);
			if(!empty($prev_post)) {
				$title=$prev_post->post_title;
				$title= substr($title, 0, 20)."..."; 
				echo '<a href="' . get_permalink($prev_post->ID) . '" title="' . $prev_post->post_title . '" class="forward condensed sub1a blue1 upp smooth">' . $title . '</a>'; }
          ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>


<script type="text/javascript">
$(document).ready(function(){ 
    $("#nc").validate();
    $('#nc').ajaxForm(function() { 
        $(".subcom").text('Thanks!');
        $("#commentor").val('');
        $("#comment").val('');
      }); 
    $('textarea').autosize(); 
    $("#handle").click(function() {
        $("#share").slideToggle("slow");
        $("div#handle").toggleClass("active")
    }); 	
});
</script>
<script type="text/javascript">
jQuery(document).ready(function($) {
$('#commentform').validate({
 
rules: {
  author: {
    required: true,
    minlength: 2
  },
 
  email: {
    required: true,
    email: true
  },
 
  comment: {
    required: true,
    minlength: 20
  }
},
 
messages: {
  author: "Please enter in your name.",
  email: "Please enter a valid email address.",
  comment: "Message box can't be empty!"
},
 
errorElement: "div",
errorPlacement: function(error, element) {
  element.before(error);
}
 
});
});
</script>
<script type="text/javascript"> 
    $(document).ready(function() { 
      
      var $container = $('#indexProductList');

      $container.isotope({
        itemSelector : '.productlisting',		
		onLayout: function( elems, instance ) {		
			$('.productlisting:not(.isotope-hidden)').each(function(index){				
				$(this).removeClass (function (i, css) {    
					return (css.match (/\bbox\S+/g) || []).join(' ');
				});					
				$(this).addClass("box"+index);						
				if(index%3!=0)				
				$(this).removeClass("line").addClass("line");				
				else				
				$(this).removeClass("line");				
				//console.log($( window ).width());									
			});				
		}
      });
      
      var $optionSets = $('#refine'),
          $optionLinks = $optionSets.find('a');

      $optionLinks.click(function(){
        var $this = $(this);
        // don't proceed if already selected
        if ( $this.hasClass('selected') ) {
          return false;
        }
        var $optionSet = $this.parents('.option-set');
        $optionSet.find('.selected').removeClass('selected');
        $this.addClass('selected');
  
        // make option object dynamically, i.e. { filter: '.my-filter-class' }
        var options = {},
            key = $optionSet.attr('data-option-key'),
            value = $this.attr('data-option-value');
        // parse 'false' as false boolean
        value = value === 'false' ? false : value;
        options[ key ] = value;		 
        if ( key === 'layoutMode' && typeof changeLayoutMode === 'function' ) {
          // changes in layout modes need extra logic
          changeLayoutMode( $this, options )		  		 
        } else {
          // otherwise, apply new options		  		 
          $container.isotope( options );
        }
        
        return false;
      });

	 });

</script> 

 <script type="text/javascript"> 
        $(document).ready(function() { 
		
			$("#ref").click(function() {
				$("div#ref").toggleClass("active")
				$("#refine").slideToggle("slow");
			}); 
			
			$("#ref,#refine").hover(function() {
				$("div#ref").toggleClass("active")
			}); 
		
			$('#ref,#refine').hover(function() {
				$('#refine').stop().fadeIn();
			}, function(){
				$('#refine').stop().fadeOut();
			});
			
			$('.platbb').click(function(){
				$(this).addClass("clicked");
			});
			
            $('.platb').ajaxForm(function() { 
				$(".clicked").text('Added to Bag!');
				$("#bag").load('ajax-cart-count.html');
            }); 
			
        }); 
</script>
<?php get_footer(); ?>
