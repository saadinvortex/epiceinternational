<?php
/**
 * Template Name: Store Locator
 *
 * Display Store Locator address.
 */
?>
<?php get_header(); ?>

<div id="whitebg">
    <div id="mainWrapperSingle">
        <div id="mainWrapper">
            <div id="contentMainWrapper">
                <div id="newtop" class="cat"> 

                    <!-- bof upload alerts --> 
                    <!-- eof upload alerts -->

                    <div class="centerColumn locator" id="ezPageDefault"> 

                        <!-- THIS IS THE 'STORE LOCATOR' PAGE -->

                        <h1 class="condensed head1 blue1">Find an &Eacute;pic&eacute; retailer near you</h1>
                        <script type="text/javascript">
                            $(document).ready(function(){

                                $('.wrap').each(function(){  

                                    var highestBox = 0;
                                    $('.location', this).each(function(){

                                        if($(this).height() > highestBox) 
                                            highestBox = $(this).height(); 
                                    });  

                                    $('.location',this).height(highestBox);

                                }); 


                                $('#sltabs div.wrap').hide();
                                $('#sltabs div#tab-1').fadeIn();
                                $('#sltabs ul li:first').addClass('active');
		 
                                $('#sltabs ul li a').click(function(){
                                    $('#sltabs ul li').removeClass('active');
                                    $(this).parent().addClass('active');
                                    var currentTab = $(this).attr('href');
                                    $(currentTab).hide();
                                    $('#sltabs div.wrap').slideUp("slow");
                                    $(currentTab).slideDown("slow");
                                    return false;
                                }); 

                            });
                        </script>
                        <div id="sltabs">
                            <ul>
                                <li class="col-1-3 condensed head2"> <a href="#tab-1" class="s">
                                        <div class="fade"> <img src="<?php echo get_template_directory_uri(); ?>/images/north-america.jpg" alt="" class="act" /> <img src="<?php echo get_template_directory_uri(); ?>/images/north-america-default.jpg" alt="" class="default" /> </div>
                                        <span class="smooth s">North America</span> <span data-icon="&#xe003;" aria-hidden="true" class="down blue1 smooth"></span> </a> </li>
                                <li class="col-1-3 condensed head2"> <a href="#tab-2" class="s">
                                        <div class="fade"> <img src="<?php echo get_template_directory_uri(); ?>/images/caribbean.jpg" alt="" class="act" /> <img src="<?php echo get_template_directory_uri(); ?>/images/caribbean-default.jpg" alt="" class="default" /> </div>
                                        <span class="smooth s">Caribbean</span> <span data-icon="&#xe003;" aria-hidden="true" class="down blue1 smooth"></span> </a> </li>
                                <li class="col-1-3 condensed head2"> <a href="#tab-3" class="s">
                                        <div class="fade"> <img src="<?php echo get_template_directory_uri(); ?>/images/europe.jpg" alt="" class="act" /> <img src="<?php echo get_template_directory_uri(); ?>/images/europe-default.jpg" alt="" class="default" /> </div>
                                        <span class="smooth s">Europe and United Kingdom</span> <span data-icon="&#xe003;" aria-hidden="true" class="down blue1 smooth"></span> </a> </li>
                            </ul>


                            <div id="tab-1" class="wrap">
                                <?php
                                //Store Locator North America
                                $args = array('post_type' => 'north america', 'order' => 'ASC');
                                $loop = new WP_Query($args);
                                while ($loop->have_posts()) : $loop->the_post();
                                    ?>
                                    <?php $phoneNumber = get_post_meta(get_the_ID(), 'PhoneNumber', true); ?>
                                    <?php $website = get_post_meta(get_the_ID(), 'website', true); ?>
                                    <div class="col-1-3 location count6">
                                        <p> <strong> <?php the_title(); ?></strong> <?php the_content(); ?></p>

                                        <p class="storetel s"><?php echo $phoneNumber; ?>&nbsp;&nbsp;&nbsp;&nbsp;
                                            <?php if (!empty($website))
                                            { ?>
                                                <a href="<?php echo $website; ?>" target="_blank" class="s storeurl">
                                                    <?php echo $website; ?>
                                                </a>
                                            <?php } 
                                            $content = get_the_content();
                                            $content = str_replace(" ","+",$content);
                                            
                                            ?>
                                        </p>
                                        <a href="https://maps.google.com/?q=<?php echo $content;?>" target="_blank" class="blue1 smooth">view on map</a>
                                    </div>
                                <?php endwhile; ?>
                            </div>


                            <div id="tab-2" class="wrap">

                                <?php
                                //Store Locator Caribbean
                                $count_barbados = 1;  //CSS class
                                $count = 1;  //CSS class

                                $args = array('post_type' => 'caribbean', 'order' => 'ASC');
                                $loop = new WP_Query($args);
                                while ($loop->have_posts()) : $loop->the_post();

                                    $location = get_post_meta(get_the_ID(), 'Location', true);
                                    $phoneNumber = get_post_meta(get_the_ID(), 'PhoneNumber', true);
                                    $website = get_post_meta(get_the_ID(), 'website', true);
                                    if ($location == 'Barbados')
                                    {
                                        ?>
                                        <?php if (!$i++)
                                            echo " <h4 class='condensed upp head2'>Barbados</h4>"; ?>
                                        <div class="col-1-3 location count<?php echo $count_barbados; ?>">
                                            <p> <strong> <?php the_title(); ?></strong> <?php the_content(); ?></p>
                                            <p class="storetel s">
                                                <?php echo $phoneNumber; ?>&nbsp;&nbsp;&nbsp;&nbsp;
                                                <?php if (!empty($website))
                                                { ?>
                                                    <a href="<?php echo $website; ?>" target="_blank" class="s storeurl">
                                                        <?php echo $website; ?>
                                                    </a>
                                                <?php } ?>
                                            </p>
                                            <a href="https://maps.google.com/?q=8059%20Spyglass%20Hill%20Rd.#103 Viera, FL 32940" target="_blank" class="blue1 smooth">view on map</a>               			 </div>
                                    <?php } //end If ?>


                                    <?php if ($location == 'Trinidad')
                                    { ?>
                                        <?php if (!$j++)
                                            echo " <h4 class='condensed upp head2'>Trinidad</h4>"; ?>
                                        <div class="col-1-3 location count<?php echo $count; ?>">
                                            <p> <strong> <?php the_title(); ?></strong> <?php the_content(); ?></p>
                                            <p class="storetel s">
                                                <?php echo $phoneNumber; ?>&nbsp;&nbsp;&nbsp;&nbsp;
                                                <?php if (!empty($website))
                                                { ?>
                                                    <a href="<?php echo $website; ?>" target="_blank" class="s storeurl">
                                                        <?php echo $website; ?>
                                                    </a>
                                                <?php } ?>
                                            </p>
                                            <a href="https://maps.google.com/?q=8059%20Spyglass%20Hill%20Rd.#103 Viera, FL 32940" target="_blank" class="blue1 smooth">view on map</a>               			 </div>

                                        <?php
                                    }  // End if
                                    $count_trinidad++;
                                    $count_barbados++;
                                endwhile;
                                ?>
                            </div>



                            <div id="tab-3" class="wrap">
                                <?php
                                //Store Locator Europe & UK
                                $args = array('post_type' => 'europe and united ki', 'order' => 'ASC');
                                $loop = new WP_Query($args);
                                while ($loop->have_posts()) : $loop->the_post();
                                    ?>
                                    <?php $phoneNumber = get_post_meta(get_the_ID(), 'PhoneNumber', true); ?>
                                    <?php $website = get_post_meta(get_the_ID(), 'website', true); ?>
                                    <div class="col-1-3 location count6">
                                        <p> <strong> <?php the_title(); ?></strong> <?php the_content(); ?></p>

                                        <p class="storetel s"><?php echo $phoneNumber; ?>&nbsp;&nbsp;&nbsp;&nbsp;
                                            <?php if (!empty($website))
                                            { ?>
                                                <a href="<?php echo $website; ?>" target="_blank" class="s storeurl">
                                                    <?php echo $website; ?>
                                                </a>
                                            <?php } ?>
                                        </p>
                                        <a href="https://maps.google.com/?q=8059%20Spyglass%20Hill%20Rd.#103 Viera, FL 32940" target="_blank" class="blue1 smooth">view on map</a>
                                    </div>
                                <?php endwhile; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    var nav = responsiveNav(".nav-collapse", { 
        customToggle: "#toggle", // Selector: Specify the ID of a custom toggle
        insert: "after",
    });
</script>
</body><!-- Mirrored from www.epiceinternational.com/store-locator by HTTrack Website Copier/3.x [XR&CO'2013], Tue, 11 Mar 2014 10:12:04 GMT -->
</html><?php get_footer(); ?>
