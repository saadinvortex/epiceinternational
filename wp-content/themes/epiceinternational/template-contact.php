<?php
/**
 * Template Name: Contact Us
 *
 * Display Testimonials.
 * Display Submit Testimonial form.
 */
?>
<?php get_header(); ?>

<div id="whitebg">
    <div id="mainWrapperSingle">
        <div id="mainWrapper">
            <div id="contentMainWrapper">
                <div id="newtop" class="cat"> 

                    <!-- bof upload alerts --> 
                    <!-- eof upload alerts -->

                    <div class="centerColumn" id="contactUsDefault">
                        <div class="col-2-5">
                            <h1 class="condensed blue1 head1">Say Hello</h1>
                            <strong class="sub1">Epice International</strong>
                            <address class="sub1">
                                <?php echo get_option('ep_address'); ?>
                            </address>
                            <p id="tel" class="s sub1"><?php echo get_option('ep_phn'); ?> <?php echo do_shortcode('[contact-form-7 id="83" title="Contact form 1"]'); ?> 
                        </div>
                        <div id="thanks" class="s">Thank you for your inquiry. Someone will respond to you within 48 hours.</div>
                        <script type="text/javascript">
                            $(document).ready(function() {
                                $("#cus").validate({
                                    rules: {
                                        contactname:{required:true,minlength:2},
                                        enquiry: {required:true,minlength:50},
                                        email: {
                                            required: true,
                                            email: true
                                        },
                                    },
                                    messages: {
                                        contactname: "Please enter your name",
                                        enquiry: "Please enter 50 characters or more",
                                        email: "Please enter a valid email address, i.e. johnsmith@gmail.com",
                                    }
                                });
	
                                $("#cus").ajaxForm(function() { 
                                    $(".pseudo").text('Thanks!');
                                    $("#cus").slideUp("slow");
                                    $("#thanks").slideDown("slow");
                                }); 
                            });
                        </script> 
                    
                    <div class="col-3-5">
                        <h1 class="condensed blue1 head1">OUR OFFICE</h1>
                        <?php
                        $address = get_option('ep_address'); // Google HQ
                        $prepAddr = str_replace(' ', '+', $address);
                        $geocode = file_get_contents('http://maps.google.com/maps/api/geocode/json?address=' . $prepAddr . '&sensor=false');
                        $output = json_decode($geocode);
                        $lat = $output->results[0]->geometry->location->lat;
                        $long = $output->results[0]->geometry->location->lng;
                        echo $ifram = "<iframe width='100%' height='500px' frameborder='0' scrolling='no' marginheight='0' marginwidth='0' src='https://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=+&amp;q=" . $address . "&amp;ie=UTF8&amp;hq=&amp;hnear=" . $address . "&amp;t=m&amp;z=14&amp;ll= " . $lat . "," . $long . "&amp;output=embed&amp;iwloc=near'></iframe>";
                        ?>

                    </div>
                        </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php get_footer(); ?>
