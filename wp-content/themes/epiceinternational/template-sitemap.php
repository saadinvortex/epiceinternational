<?php
/**
 * Template Name: Site Map
 *
 * Display Testimonials.
 * Display Submit Testimonial form.
 */
get_header(); ?>

<div id="whitebg">
  <div id="mainWrapperSingle">
    <div id="mainWrapper">
    <div id="contentMainWrapper">
      <div id="newtop" class="cat">
			<div class="centerColumn" id="siteMap">

<div id="siteMapMainContent" class="content">

</div>

<div class="col-1-4">
	<strong class="condensed blue1 head1">General</strong>
	<ul>
		<li><a href="<?php echo site_url(); ?>">Epice Home</a></li>
		<li><a href="<?php echo esc_url( get_permalink( get_page_by_title( 'subscribe to our newsletter' ) ) ); ?>">Subscribe to our newsletter</a></li>
	</ul>
	<strong class="condensed blue1 head1">About Epice</strong>
	<ul>
		<li><a href="<?php echo esc_url( get_permalink( get_page_by_title( 'about epice' ) ) ); ?>">About Epice</a></li>
	</ul>
	<strong class="condensed blue1 head1">Media &amp; News</strong>
        <?php 
        $args = array('post_type=post&post_status=publish');
                            $the_query = new WP_Query($args);
                            if ($the_query->have_posts()) :
                                while ($the_query->have_posts()) : $the_query->the_post();
                                    ?>
        <ul>
		<li><a href="<?php echo the_permalink();?>"><?php echo the_title();?></a></li>
        </ul>
         <?php endwhile; ?>
                                <?php wp_reset_postdata(); ?>

                            <?php else: ?>
                                <p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
                            <?php endif; ?>
	
	<strong class="condensed blue1 head1">Testimonials</strong>
	<ul>
		<li><a href="<?php echo esc_url( get_permalink( get_page_by_title( 'testimonials' ) ) ); ?>">Testimonials</a></li>
	</ul>
</div>

<div class="col-1-4">
	<strong class="condensed blue1 head1">Customer Service</strong>
	<ul>
		<li><a href="<?php echo esc_url( home_url( '/' ) ); ?>store-locator">Store Locator</a></li>
		<li><a href="<?php echo esc_url( home_url( '/' ) ); ?>our-policies">Our Policies</a></li>
		<li><a href="<?php echo esc_url( home_url( '/' ) ); ?>shipping-and-returns">Shipping and Returns</a></li>
		<li><a href="<?php echo esc_url( home_url( '/' ) ); ?>contact-us">Contact Us</a></li>
		<li><a href="<?php echo esc_url( home_url( '/' ) ); ?>site-map">Sitemap</a></li>
	</ul>
</div>

<div class="col-1-4">

		<strong class="condensed blue1 head1"><a href="<?php echo esc_url( home_url( '/' ) ); ?>skincare" class="blue1">Skincare</a></strong>
                <ul>
                <?php
                wp_reset_query();
                $args = array('wpsc_product_category' => 'skincare', 'post_status'=>'publish', 'post_type'=>'wpsc-product');
                $the_query = new WP_Query($args);
                if ($the_query->have_posts()) :
                while ($the_query->have_posts()) : $the_query->the_post();
                
                ?>
                <li><a href="<?php echo the_permalink();?>"><?php echo the_title();?></a></li>
                <?php
                endwhile;
                else:             
                endif;
?>
                    
                </ul>
                <strong class="condensed blue1 head1"><a href="<?php echo esc_url( home_url( '/' ) ); ?>moisturizers" class="blue1">Moisturizers</a></strong>
                <ul>
                    <?php
                wp_reset_query();
                $args = array('wpsc_product_category' => 'moisturizers', 'post_status'=>'publish', 'post_type'=>'wpsc-product');
                $the_query = new WP_Query($args);
                if ($the_query->have_posts()) :
                while ($the_query->have_posts()) : $the_query->the_post();
                
                ?>
                <li><a href="<?php echo the_permalink();?>"><?php echo the_title();?></a></li>
                <?php
                endwhile;
                else:             
                endif;
?>
                    
                </ul>
                <strong class="condensed blue1 head1"><a href="<?php echo esc_url( home_url( '/' ) ); ?>sunscreen" class="blue1">Sunscreen</a></strong>
                <ul>
                    <?php
                wp_reset_query();
                $args = array('wpsc_product_category' => 'sunscreen', 'post_status'=>'publish', 'post_type'=>'wpsc-product');
                $the_query = new WP_Query($args);
                if ($the_query->have_posts()) :
                while ($the_query->have_posts()) : $the_query->the_post();
                
                ?>
                <li><a href="<?php echo the_permalink();?>"><?php echo the_title();?></a></li>
                <?php
                endwhile;
                else:             
                endif;
?>
                </ul></div>

<div class="col-1-4">
	<strong class="condensed blue1 head1">Social Media</strong>
	<ul>
		<li><a href="<?php echo get_option('ep_fb'); ?>" target="_blank">Epice Facebook</a></li>
		<li><a href="<?php echo get_option('ep_tw'); ?>" target="_blank">Epice Twitter</a></li>
		<li><a href="<?php echo get_option('ep_yt'); ?>" target="_blank">Epice YouTube</a></li>
		<li><a href="<?php echo get_option('ep_pt'); ?>" target="_blank">Epice Pinterest</a></li>
	</ul>
	<strong class="condensed blue1 head1">Shopping Bag</strong>
	<ul>
					<li><a href="<?php echo esc_url( home_url( '/' ) ); ?>login">Login/Register</a></li>
					</ul>
</div>





</div>
            
            </div>
         </div>
	</div>
</div>
</div>

<?php

get_footer();
