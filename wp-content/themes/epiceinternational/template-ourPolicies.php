<?php
/**
 * Template Name: Our Policies
 *
 * Display Policy page content.
 */
?>
<?php get_header(); ?>

<div id="whitebg">
    <div id="mainWrapperSingle">
        <div id="mainWrapper">
            <div id="contentMainWrapper">
                <div id="newtop" class="cat1"> 

                    <!-- bof upload alerts --> 
                    <!-- eof upload alerts -->

                    <div class="centerColumn" id="privacy">
                        <h1 id="privacyDefaultHeading">Conditions of Use</h1>
                        <div id="privacyDefaultMainContent" class="content">
                            <p><strong>Company Name:</strong>   <?php echo get_option('ep_company'); ?><br />
                                <strong>Address:</strong>  <?php echo get_option('ep_address'); ?><br />
                                <strong>City, State, Zip:</strong>  <?php echo get_option('ep_state'); ?><br />
                                <strong>e-mail:</strong> <a href="mailto:<?php echo get_option('ep_email'); ?>" class="blue1"> <?php echo get_option('ep_email'); ?></a><br />
                                <strong>Phone:</strong>  <?php echo get_option('ep_phn'); ?><br />
                                <strong>Office Hours:</strong>  <?php echo get_option('ep_hours'); ?><br />
                                <strong>Effective date of policy:</strong>   <?php echo get_option('ep_dateOfPolicy'); ?></p>
                            <?php
                            // page Content 
                            while (have_posts()) : the_post();
                                get_template_part('content', 'page');
                            endwhile;
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php get_footer(); ?>
