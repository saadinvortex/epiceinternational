<?php
/**
 * The template for displaying Category pages
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>

	<section id="primary" class="content-area">
		<div id="content" class="site-content" role="main">

			<?php if ( have_posts() ) : ?>

			<header class="archive-header">
				<h1 class="archive-title"><?php printf( __( 'Category Archives: %s', 'twentyfourteen' ), single_cat_title( '', false ) ); ?></h1>

				<?php
					// Show an optional term description.
					$term_description = term_description();
					if ( ! empty( $term_description ) ) :
						printf( '<div class="taxonomy-description">%s</div>', $term_description );
					endif;
				?>
			</header><!-- .archive-header -->

			<?php
					// Start the Loop.
					while ( have_posts() ) : the_post();

					/*
					 * Include the post format-specific template for the content. If you want to
					 * use this in a child theme, then include a file called called content-___.php
					 * (where ___ is the post format) and that will be used instead.
					 */
					get_template_part( 'content', get_post_format() );

					endwhile;
					// Previous/next page navigation.
					twentyfourteen_paging_nav();

				else :
					// If no content, include the "No posts found" template.
					get_template_part( 'content', 'none' );

				endif;
			?>
		</div><!-- #content -->
	</section><!-- #primary -->
<script type="text/javascript"> 
    $(document).ready(function() { 
      
      var $container = $('#indexProductList');

      $container.isotope({
        itemSelector : '.productlisting',		
		onLayout: function( elems, instance ) {		
			$('.productlisting:not(.isotope-hidden)').each(function(index){				
				$(this).removeClass (function (i, css) {    
					return (css.match (/\bbox\S+/g) || []).join(' ');
				});					
				$(this).addClass("box"+index);						
				if(index%3!=0)				
				$(this).removeClass("line").addClass("line");				
				else				
				$(this).removeClass("line");				
				//console.log($( window ).width());									
			});				
		}
      });
      
      var $optionSets = $('#refine'),
          $optionLinks = $optionSets.find('a');

      $optionLinks.click(function(){
        var $this = $(this);
        // don't proceed if already selected
        if ( $this.hasClass('selected') ) {
          return false;
        }
        var $optionSet = $this.parents('.option-set');
        $optionSet.find('.selected').removeClass('selected');
        $this.addClass('selected');
  
        // make option object dynamically, i.e. { filter: '.my-filter-class' }
        var options = {},
            key = $optionSet.attr('data-option-key'),
            value = $this.attr('data-option-value');
        // parse 'false' as false boolean
        value = value === 'false' ? false : value;
        options[ key ] = value;		 
        if ( key === 'layoutMode' && typeof changeLayoutMode === 'function' ) {
          // changes in layout modes need extra logic
          changeLayoutMode( $this, options )		  		 
        } else {
          // otherwise, apply new options		  		 
          $container.isotope( options );
        }
        
        return false;
      });

	 });

</script> 

 <script type="text/javascript"> 
        $(document).ready(function() { 
		
			$("#ref").click(function() {
				$("div#ref").toggleClass("active")
				$("#refine").slideToggle("slow");
			}); 
			
			$("#ref,#refine").hover(function() {
				$("div#ref").toggleClass("active")
			}); 
		
			$('#ref,#refine').hover(function() {
				$('#refine').stop().fadeIn();
			}, function(){
				$('#refine').stop().fadeOut();
			});
			
			$('.platbb').click(function(){
				$(this).addClass("clicked");
			});
			
            $('.platb').ajaxForm(function() { 
				$(".clicked").text('Added to Bag!');
				$("#bag").load('ajax-cart-count.html');
            }); 
			
        }); 
</script>
<?php
get_sidebar( 'content' );
get_sidebar();
get_footer();
