<?php
// Setup globals
// @todo: Get these out of template
global $wp_query;

// Setup image width and height variables
// @todo: Investigate if these are still needed here
$image_width = get_option('single_view_image_width');
$image_height = get_option('single_view_image_height');

function tmp($entry) {
    return $entry['name'];
}
?>
<?php while (wpsc_have_products()) : wpsc_the_product(); ?>
    <div id="whitebg">
        <div id="mainWrapperSingle">
            <div id="mainWrapper">
                <div id="contentMainWrapper">
                    <div id="newtop" class="cat1"> 

                        <!-- bof upload alerts --> 
                        <!-- eof upload alerts --> 

                        <script type="text/javascript"> 
                            $(document).ready(function() { 
                                $('#atsb').ajaxForm(function() { 
                                    $(".button_in_cart").text('Added to Shopping Bag');
                                    $(".button_in_cart").addClass('noafter');
                                    $("#bag").load('../../ajax-cart-count.html');
                                }); 
                                $("#handle").click(function() {
                                    $("#share").slideToggle("slow");
                                    $("div#handle").toggleClass("active")
                                }); 
                            }); 
                        </script>
                        <div class="centerColumn" id="productGeneral"> 



                            <div class="col-35"> 

                                <!--bof Main Product Image -->
                                <div class="moisturizers">
                                    <div id="productMainImage">
                                        <?php if (wpsc_the_product_thumbnail()) : ?>
                                            <a rel="<?php echo wpsc_the_product_title(); ?>" class="<?php echo wpsc_the_product_image_link_classes(); ?>" href="<?php echo esc_url(wpsc_the_product_image()); ?>">
                                                <img class="product_image" id="product_image_<?php echo wpsc_the_product_id(); ?>" alt="<?php echo wpsc_the_product_title(); ?>" title="<?php echo wpsc_the_product_title(); ?>" src="<?php echo wpsc_the_product_thumbnail(); ?>"/>
                                            </a>
                                            <?php
                                            if (function_exists('gold_shpcrt_display_gallery'))
                                                echo gold_shpcrt_display_gallery(wpsc_the_product_id());
                                            ?>
                                        <?php else: ?>
                                            <a href="<?php echo esc_url(wpsc_the_product_permalink()); ?>">
                                                <img class="no-image" id="product_image_<?php echo wpsc_the_product_id(); ?>" alt="No Image" title="<?php echo wpsc_the_product_title(); ?>" src="<?php echo WPSC_CORE_THEME_URL; ?>wpsc-images/noimage.png" width="<?php echo get_option('product_image_width'); ?>" height="<?php echo get_option('product_image_height'); ?>" />
                                            </a>
                                        <?php endif; ?>

                                    </div>
                                </div>
                                <!--eof Main Product Image--> 

                                <!--bof Add to Cart Box -->

                                <div id="cartAdd">
                                    <form class="product_form" enctype="multipart/form-data" action="<?php echo esc_url(wpsc_this_page_url()); ?>" method="post" name="1" id="product_<?php echo wpsc_the_product_id(); ?>">
                                        <?php
                                        /**
                                         * Quantity options - MUST be enabled in Admin Settings
                                         */
                                        ?>
                                        <?php if (wpsc_has_multi_adding()): ?>
                                            <input type="number" id="wpsc_quantity_update_<?php echo wpsc_the_product_id(); ?>"  name="wpsc_quantity_update" class="productQty" size="2" value="1" />
                                            <input type="hidden" name="key" value="<?php echo wpsc_the_cart_item_key(); ?>"/>
                                            <input type="hidden" name="wpsc_update_quantity" value="true" />
                                            <input type="hidden" value="add_to_cart" name="wpsc_ajax_action" />
                                            <input type="hidden" value="<?php echo wpsc_the_product_id(); ?>" name="product_id" />
                                            <input type="submit" value="<?php _e('Add to shopping bag', 'wpsc'); ?>" name="Buy" class="pseudo proBtn button_in_cart" id="product_<?php echo wpsc_the_product_id(); ?>_submit_button"/>

                                        <?php endif; ?>
                                    </form>
                                </div>
                                <!--eof Add to Cart Box-->

                                <div id="addthis">
                                    <div id="handle" class="sub1a smooth condensed main upp"><span>Share Product</span> <span id="si" class="smooth"></span></div>
                                    <div id="share">
                                        <div class="addthis_toolbox addthis_default_style addthis_16x16_style">
                                            <a class="addthis_button_twitter"><span class="st-twitter"></span></a> 
                                            <a class="addthis_button_facebook"><span class="st-facebook"></span></a> 
                                            <a class="addthis_button_google_plusone_share"><span class="st-googleplus"></span></a>
                                            <a class="addthis_button_pinterest_share"><span class="st-pinterest"></span></a>
                                            <a class="addthis_button_email"><span class="st-email"></span></a> </div>
                                    </div>
                                </div>
                                <script type="text/javascript" src="../../../s7.addthis.com/js/300/addthis_widget.js#pubid=xa-5294746568c2de07"></script> 
                            </div>
                            <div class="col-60"> 

                                <!--bof Product Name-->
                                <h1 id="productName" class="condensed head1"><?php echo wpsc_the_product_title(); ?><span class="s">(1.0 oz)</span></h1>
                                <!--eof Product Name-->
<?php   
             
                $categories = wp_get_object_terms (wpsc_the_product_id(), 'wpsc_product_category');
                $tmp = json_decode(json_encode($categories), true);
                
                $slug= array_map("tmp", $tmp);
                //$catName = explode

            ?>
                                <p class="catname moisturizers sub1">  <?php echo $slug[0];?></p>



                                <!--bof Product Price block -->
                                <h2 id="productPrices" class="head1 condensed upp">
                                    <span class="back"> 
                                        <?php wpsc_the_product_price_display(); ?>
                                        <!-- multi currency code -->
                                        <?php if (wpsc_product_has_multicurrency()) : ?>
                                            <?php echo wpsc_display_product_multicurrency(); ?>
                                        <?php endif; ?>
                                        <?php if (wpsc_show_pnp()) : ?>
                                            <p class="pricedisplay"><?php _e('Shipping', 'wpsc'); ?>:<span class="pp_price"><?php echo wpsc_product_postage_and_packaging(); ?></span></p>
                                        <?php endif; ?>
                                    </span>
                                    <span id="stockline"></span>
                                    <span id="stock" class="s">
                                        <?php if (wpsc_show_stock_availability()): ?>
                                            <?php if (wpsc_product_has_stock()) : ?>
                                                <?php _e('in stock', 'wpsc'); ?>
                                            <?php else: ?>
                                                <?php _e('Not in stock', 'wpsc'); ?>
                                            <?php endif; ?>
                                        <?php endif; ?>
                                    </span>
                                </h2>
                                <!--eof Product Price block --> 

                                <!--bof free ship icon  --> 
                                <!--eof free ship icon  --> 

                                <!--bof Product description -->
                                <div id="productDescription">
                                    <p><?php echo wpsc_the_product_description(); ?></p>
                                </div>
                                <!--eof Product description --> 

                                <br class="clearBoth" />

                                <!--bof Product details list  --> 
                                <!--eof Product details list --> 

                                <!--bof Attributes Module --> 
                                <!--eof Attributes Module --> 

                                <!--bof Quantity Discounts table --> 
                                <!--eof Quantity Discounts table --> 

                                <!--bof Additional Product Images --> 
                                <!--eof Additional Product Images --> 

                                <!--bof Prev/Next bottom position --> 
                                <!--eof Prev/Next bottom position --> 

                                <!--bof Reviews button and count--> 
                                <!--eof Reviews button and count --> 

                                <!--bof Product date added/available--> 
                                <!--eof Product date added/available --> 

                                <!--bof Product URL --> 
                                <!--eof Product URL --> 

                            </div>

                            <!--bof Form close-->
                            </form>
                            <!--bof Form close--> 

                            <!--bof also purchased products module--> 
                        <?php endwhile; ?>
                        <script type="text/javascript"> 
                            $(document).ready(function() { 
                                $('.platbb').click(function(){
                                    $(this).addClass("clicked");
                                });
                                $('.platb').ajaxForm(function() { 
                                    $(".clicked").text('Added to Bag!');
                                    $("#bag").load('../../ajax-cart-count.html');
                                }); 	
                            }); 
                        </script>
                        <div class="centerBoxWrapper" id="alsoPurchased">
                            <?php on_wpec_related() ?>
                        </div>
                        <!--eof also purchased products module--> 

                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
