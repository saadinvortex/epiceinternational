<?php
/**
 * Template Name: Testimonial
 *
 * Display Testimonials.
 * Display Submit Testimonial form.
 */
?>
<?php get_header(); ?>

<div id="whitebg">
    <div id="mainWrapperSingle">
        <div id="mainWrapper">
            <div id="contentMainWrapper">
                <div id="newtop" class="cat"> 

                    <!-- bof upload alerts --> 
                    <!-- eof upload alerts -->

                    <div class="centerColumn testimonials" id="ezPageDefault"> 

                        <!-- THIS IS THE 'TESTIMONIALS' PAGE -->

                        <h1 class="condensed blue1 head1 upp">See what people are saying</h1>
                        <script type="text/javascript">
                            $(document).ready(function() {

                                var track_click = 0; //track user click on "load more" button, righ now it is 0 click
	
                                var total_pages = 5;
                                $('#results').load("ajax-testimonials.php", {'page':track_click}, function() {track_click++;}); //initial data to load

                                $(".load_more").click(function (e) { //user clicks on button
	
                                    $(this).hide(); //hide load more button on click
                                    $('.animation_image').show(); //show loading image

                                    if(track_click <= total_pages) //make sure user clicks are still less than total pages
                                    {
                                        //post page number and load returned data into result element
                                        $.post('ajax-testimonials.php',{'page': track_click}, function(data) {
			
                                            $(".load_more").show(); //bring back load more button
				
                                            $("#results").append(data); //append data received from server
				
                                            //scroll page to button element
                                            //$("html, body").animate({scrollTop: $("#load_more_button").offset().top}, 500);
				
                                            //hide loading image
                                            $('.animation_image').hide(); //hide loading image once data is received
	
                                            track_click++; //user click increment on load button
			
                                        }).fail(function(xhr, ajaxOptions, thrownError) { 
                                            alert(thrownError); //alert any HTTP error
                                            $(".load_more").show(); //bring back load more button
                                            $('.animation_image').hide(); //hide loading image once data is received
                                        });
			
			
                                        if(track_click >= total_pages-1)
                                        {
                                            //reached end of the page yet? disable load button
                                            $(".load_more").text('No more Testimonials');
                                            $(".load_more").addClass('nmt');
                                        }
                                    }
		  
                                });
		
                                $('textarea').autosize();  
                                $("#nt").validate({
                                    submitHandler: function(form) {
                                        $(form).ajaxSubmit(function() { 
                                            $(".subtest").text('Thanks!');
                                            $("#nt").slideUp("slow");
                                            $("#thanks").slideDown("slow");
                                        }); 
                                    }
                                });
		
		
                            });
                        </script>
                        <div id="results" style="overflow:hidden;">
                            <div class="col-1-2 testl">
                       <?php /*?> <?php
                        $the_query = new WP_Query(array('post_type' => 'testimonial', 'post_status' => 'publish', 'orderby' => 'date', 'order' => 'ASC'));

                        // The Loop
                        if ($the_query->have_posts()) :
                            while ($the_query->have_posts()) : $the_query->the_post();
                                ?>
                               <div class=" test1">
                                    <div class="speak"><?php echo the_content();?></div>
                                    <div class="testitri"></div>
                                    <div class="nom"><?php the_author(); ?></div>
                                    
                                </div>
                                <?php
                            endwhile;
                        endif;

                        // Reset Post Data
                        wp_reset_postdata();
                        ?><?php */?>
                        
                        
                                
</div>
                        <br class="clearBoth" />
                        <?php echo do_shortcode('[full-testimonials category="xx"]'); ?>
                        <div align="center"> <a class="load_more condensed blue1 sub1a upp" id="load_more_button">Load More</a>
                            <div class="animation_image condensed blue1 sub1a upp" style="display:none;">Loading...</div>
                        </div>
                        <?php echo do_shortcode('[testimonial-form]') ?> </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php get_footer(); ?>
