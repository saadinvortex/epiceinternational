<?php
/**
 * Template Name: Login Registration
 *
 * Template for user login and registration
 */
get_header();
?>

<div id="whitebg">
    <div id="mainWrapperSingle">
        <div id="mainWrapper">
            <div id="contentMainWrapper">
                <div id="newtop" class="cat">
                    
                    <?php
                    if(isset($_GET['registration']) && $_GET['registration'] == 'complete')
                    {
                        echo "Login and Continue with the billing";
                        echo do_shortcode('[theme-my-login ]');
                    }
                    else if(isset($_GET['action']) && $_GET['action'] == 'lostpassword')
                    {
                         echo do_shortcode('[theme-my-login default_action="lostpassword"]');
                    }
                    else
                    {
                        echo do_shortcode('[theme-my-login]');
                        echo do_shortcode('[theme-my-login default_action="register"]');
                    }
                    
					?>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
get_footer();
