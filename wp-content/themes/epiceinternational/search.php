<?php
/**
 * The template for displaying Search Results pages
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); 
function tmp($entry) {
    return $entry['name'];
}?>
<div id="whitebg">
<div id="mainWrapper">

<div id="contentMainWrapper">


 
    <div id="newtop" class="cat">


<!-- bof upload alerts -->
<!-- eof upload alerts -->

<div class="centerColumn" id="indexProductList">
          <br class="clearBoth" />
         
          <div id="productListing">
          <?php /** start the product loop here */
          ?>
		<?php while (wpsc_have_products()) :  wpsc_the_product(); ?>
          
          <?php if(wpsc_product_count() == 0):?>
			<h3><?php  _e('There are no products in this group.', 'wpsc'); ?></h3>
		<?php endif ; ?>
          
          
            <?php   
                $categories = wp_get_object_terms (wpsc_the_product_id(), 'wpsc_product_category');
                $tmp = json_decode(json_encode($categories), true);
                
                $slug= array_map("tmp", $tmp);

            ?>
            <?php if ( get_post_type() == 'wpsc-product' ){ ?>
            <div class="<?php echo strtolower($categories[0]->name).' '. strtolower($categories[1]->name); ?> productlisting col-1-3 isotope-itema box0 smooth" ontouchstart="this.classList.toggle('hover');">
              <div class="flipper">
              
              <!--Front side product-->
                <div class="front">
                  <div class="col-1-3">
                  	<?php if(wpsc_show_thumbnails()) :?>
						<?php if(wpsc_the_product_thumbnail()) :?>
                        
							<a rel="<?php echo wpsc_the_product_title(); ?>" class="<?php echo wpsc_the_product_image_link_classes(); ?>" href="<?php echo esc_url( wpsc_the_product_image() ); ?>">
								<img class="product_image" id="product_image_<?php echo wpsc_the_product_id(); ?>" alt="<?php echo wpsc_the_product_title(); ?>" title="<?php echo wpsc_the_product_title(); ?>" src="<?php echo wpsc_the_product_thumbnail(); ?>"/>

							</a>
						<?php else: ?>
								<a href="<?php echo esc_url( wpsc_the_product_permalink() ); ?>">
								<img class="no-image" id="product_image_<?php echo wpsc_the_product_id(); ?>" alt="<?php esc_attr_e( 'No Image', 'wpsc' ); ?>" title="<?php echo wpsc_the_product_title(); ?>" src="<?php echo WPSC_CORE_THEME_URL; ?>wpsc-images/noimage.png" width="<?php echo get_option('product_image_width'); ?>" height="<?php echo get_option('product_image_height'); ?>" />
								</a>
						<?php endif; ?>
						<?php
						if(gold_cart_display_gallery()) :
							echo gold_shpcrt_display_gallery(wpsc_the_product_id(), true);
						endif;
						?>
					
				<?php endif; ?>
                  </div>
                  
                  <h3 class="itemTitle condensed head2">
                  			<?php if(get_option('hide_name_link') == 1) : ?>
								<?php echo wpsc_the_product_title(); ?>
							<?php else: ?>
								<a class="main" href="<?php echo esc_url( wpsc_the_product_permalink() ); ?>"><?php echo wpsc_the_product_title(); ?></a>
							<?php endif; ?>
                  </h3>
                  
			
                  
                  
                  <div class="onpcat"><?php echo $slug[0];?></div>
                  <a href="<?php echo esc_url( wpsc_the_product_permalink() ); ?>" class="lm blue1 condensed sub1a">Learn More</a>
                  </div>
                  <!--Front side product End-->
                  
                  
                 <!--Product backend -->
                <div class="back">
                  <h3 class="itemTitle condensed head2">
                  			<?php if(get_option('hide_name_link') == 1) : ?>
								<?php echo wpsc_the_product_title(); ?>
							<?php else: ?>
								<a class="main" href="<?php echo esc_url( wpsc_the_product_permalink() ); ?>"><?php echo wpsc_the_product_title(); ?></a>
							<?php endif; ?>
                  </h3>
                  
               <!--Product price-->
								<?php if( wpsc_show_stock_availability() ): ?>
									<?php if(wpsc_product_has_stock()) : ?>
										<div id="stock_display_<?php echo wpsc_the_product_id(); ?>" class="in_stock"><?php _e('Product in stock', 'wpsc'); ?></div>
									<?php else: ?>
										<div id="stock_display_<?php echo wpsc_the_product_id(); ?>" class="out_of_stock"><?php _e('Product not in stock', 'wpsc'); ?></div>
									<?php endif; ?>
								<?php endif; ?>
								<?php if(wpsc_product_is_donation()) : ?>
									<label for="donation_price_<?php echo wpsc_the_product_id(); ?>"><?php _e('Donation', 'wpsc'); ?>: </label>
									<input type="text" id="donation_price_<?php echo wpsc_the_product_id(); ?>" name="donation_price" value="<?php echo wpsc_calculate_price(wpsc_the_product_id()); ?>" size="6" />

								<?php else : ?>
									<?php wpsc_the_product_price_display(); ?>

									<!-- multi currency code -->
									<?php if(wpsc_product_has_multicurrency()) : ?>
	                                	<?php echo wpsc_display_product_multicurrency(); ?>
                                    <?php endif; ?>

									<?php if(wpsc_show_pnp()) : ?>
										<?php _e('Shipping', 'wpsc'); ?>:<span class="pp_price"><?php echo wpsc_product_postage_and_packaging(); ?></span>
									<?php endif; ?>
								<?php endif; ?>
                                
				 <!--Product price End-->	
                
                 <!--Product Description-->
                  <div class="listingDescription">
				  	<?php 
                                        echo substr(strip_tags(wpsc_the_product_description()), 0, 50);
                                        echo '...';
                                        //shehryar
//					   $description = substr(wpsc_the_product_description(), 0, 50);
//				       echo substr($description, 0, strrpos($description, ' '))."...";
				    ?>
                    </div>
                 <!--Product Description End-->
                 	
                  <a href="<?php echo esc_url( wpsc_the_product_permalink() ); ?>" class="lm condensed sub1a smooth">Learn More</a>
                 
                   <!--Product Add to cart button-->
                 <form class="product_form"  enctype="multipart/form-data" action="<?php echo esc_url( $action ); ?>" method="post" name="product_<?php echo wpsc_the_product_id(); ?>" id="product_<?php echo wpsc_the_product_id(); ?>" >
                   <input type="hidden" value="add_to_cart" name="wpsc_ajax_action"/>
					<input type="hidden" value="<?php echo wpsc_the_product_id(); ?>" name="product_id"/>
                    <input type="submit" value="<?php _e('Add to bag', 'wpsc'); ?>" name="Buy" class="platbb condensed sub1a smooth" id="product_<?php echo wpsc_the_product_id(); ?>_submit_button"/>
                  </form>
                  <!--Product Add to cart button End-->
                  
                </div>
                <!--Product backend  End-->
              </div>
            </div>
        
 <?php }?>
       <?php endwhile; ?>
		<?php /** end the product loop here */?>
            
            
              </div>
          </div>
    </div>
    </div>
</div>
</div>
<script type="text/javascript"> 
    $(document).ready(function() { 
      
      var $container = $('#indexProductList');

      $container.isotope({
        itemSelector : '.productlisting',		
		onLayout: function( elems, instance ) {		
			$('.productlisting:not(.isotope-hidden)').each(function(index){				
				$(this).removeClass (function (i, css) {    
					return (css.match (/\bbox\S+/g) || []).join(' ');
				});					
				$(this).addClass("box"+index);						
				if(index%3!=0)				
				$(this).removeClass("line").addClass("line");				
				else				
				$(this).removeClass("line");				
				//console.log($( window ).width());									
			});				
		}
      });
      
      var $optionSets = $('#refine'),
          $optionLinks = $optionSets.find('a');

      $optionLinks.click(function(){
        var $this = $(this);
        // don't proceed if already selected
        if ( $this.hasClass('selected') ) {
          return false;
        }
        var $optionSet = $this.parents('.option-set');
        $optionSet.find('.selected').removeClass('selected');
        $this.addClass('selected');
  
        // make option object dynamically, i.e. { filter: '.my-filter-class' }
        var options = {},
            key = $optionSet.attr('data-option-key'),
            value = $this.attr('data-option-value');
        // parse 'false' as false boolean
        value = value === 'false' ? false : value;
        options[ key ] = value;		 
        if ( key === 'layoutMode' && typeof changeLayoutMode === 'function' ) {
          // changes in layout modes need extra logic
          changeLayoutMode( $this, options )		  		 
        } else {
          // otherwise, apply new options		  		 
          $container.isotope( options );
        }
        
        return false;
      });

	 });

</script> 

 <script type="text/javascript"> 
        $(document).ready(function() { 
		
			$("#ref").click(function() {
				$("div#ref").toggleClass("active")
				$("#refine").slideToggle("slow");
			}); 
			
			$("#ref,#refine").hover(function() {
				$("div#ref").toggleClass("active")
			}); 
		
			$('#ref,#refine').hover(function() {
				$('#refine').stop().fadeIn();
			}, function(){
				$('#refine').stop().fadeOut();
			});
			
			$('.platbb').click(function(){
				$(this).addClass("clicked");
			});
			
            $('.platb').ajaxForm(function() { 
				$(".clicked").text('Added to Bag!');
				$("#bag").load('ajax-cart-count.html');
            }); 
			
        }); 
</script>
<?php
get_footer();