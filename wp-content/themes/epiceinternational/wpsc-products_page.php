<?php

$image_width = get_option('product_image_width');
/*
 * Most functions called in this page can be found in the wpsc_query.php file
 */

get_header();
function tmp($entry) {
    return $entry['name'];
}

?>

<?php wpsc_output_breadcrumbs(); ?>
<?php do_action('wpsc_top_of_products_page'); // Plugin hook for adding things to the top of the products page, like the live search ?>




                            <div id="categoryImgListing" class="categoryImg">
		<img src="<?php echo get_template_directory_uri(); ?>/images/category.png" alt="" width="940" height="296" />	</div>
        <div class="centerColumn" id="indexProductList">


          <br class="clearBoth" />
         
          <div id="productListing">
          <?php /** start the product loop here */?>

		<?php while (wpsc_have_products()) :  wpsc_the_product(); ?>
          
          <?php if(wpsc_product_count() == 0):?>
			<h3><?php  _e('There are no products in this group.', 'wpsc'); ?></h3>
		<?php endif ; ?>
          
          
             <?php   
             
                $categories = wp_get_object_terms (wpsc_the_product_id(), 'wpsc_product_category');
                $tmp = json_decode(json_encode($categories), true);
                
                $slug= array_map("tmp", $tmp);
                //$catName = explode

            ?>
            <div class="<?php echo strtolower($categories[0]->name); ?> productlisting col-1-3 isotope-itema box0 smooth" ontouchstart="this.classList.toggle('hover');">
              <div class="flipper">
              
              <!--Front side product-->
                <div class="front">
                  <div class="col-1-3">
                  	<?php if(wpsc_show_thumbnails()) :?>
						<?php if(wpsc_the_product_thumbnail()) :?>
                        
							<a rel="<?php echo wpsc_the_product_title(); ?>" class="<?php echo wpsc_the_product_image_link_classes(); ?>" href="<?php echo esc_url( wpsc_the_product_image() ); ?>">
								<img class="product_image" id="product_image_<?php echo wpsc_the_product_id(); ?>" alt="<?php echo wpsc_the_product_title(); ?>" title="<?php echo wpsc_the_product_title(); ?>" src="<?php echo wpsc_the_product_thumbnail(); ?>"/>

							</a>
						<?php else: ?>
								<a href="<?php echo esc_url( wpsc_the_product_permalink() ); ?>">
								<img class="no-image" id="product_image_<?php echo wpsc_the_product_id(); ?>" alt="<?php esc_attr_e( 'No Image', 'wpsc' ); ?>" title="<?php echo wpsc_the_product_title(); ?>" src="<?php echo WPSC_CORE_THEME_URL; ?>wpsc-images/noimage.png" width="<?php echo get_option('product_image_width'); ?>" height="<?php echo get_option('product_image_height'); ?>" />
								</a>
						<?php endif; ?>
						<?php
						if(gold_cart_display_gallery()) :
							echo gold_shpcrt_display_gallery(wpsc_the_product_id(), true);
						endif;
						?>
					
				<?php endif; ?>
                  </div>
                  
                  <h3 class="itemTitle condensed head2">
                  			<?php
                                        
                                        if(get_option('hide_name_link') == 1) : ?>
								<?php echo wpsc_the_product_title(); ?>
							<?php else: ?>
								<a class="main" href="<?php echo esc_url( wpsc_the_product_permalink() ); ?>"><?php echo wpsc_the_product_title(); ?></a>
							<?php endif; ?>
                  </h3>
                  
			
                  
                  
                  <div class="onpcat"><?php echo $slug[0];?></div>
                  <a href="<?php echo esc_url( wpsc_the_product_permalink() ); ?>" class="lm blue1 condensed sub1a">Learn More</a>
                  </div>
                  <!--Front side product End-->
                  
                  
                 <!--Product backend -->
                <div class="back">
                  <h3 class="itemTitle condensed head2">
                  			<?php if(get_option('hide_name_link') == 1) : ?>
								<?php echo wpsc_the_product_title(); ?>
							<?php else: ?>
								<a class="main" href="<?php echo esc_url( wpsc_the_product_permalink() ); ?>"><?php echo wpsc_the_product_title(); ?></a>
							<?php endif; ?>
                  </h3>
                  
               <!--Product price-->
								
								<?php if(wpsc_product_is_donation()) : ?>
									<label for="donation_price_<?php echo wpsc_the_product_id(); ?>"><?php _e('Donation', 'wpsc'); ?>: </label>
									<input type="text" id="donation_price_<?php echo wpsc_the_product_id(); ?>" name="donation_price" value="<?php echo wpsc_calculate_price(wpsc_the_product_id()); ?>" size="6" />

								<?php else : ?>
									<?php wpsc_the_product_price_display(); ?>

									<!-- multi currency code -->
									<?php if(wpsc_product_has_multicurrency()) : ?>
	                                	<?php echo wpsc_display_product_multicurrency(); ?>
                                    <?php endif; ?>

									<?php if(wpsc_show_pnp()) : ?>
										<?php _e('Shipping', 'wpsc'); ?>:<span class="pp_price"><?php echo wpsc_product_postage_and_packaging(); ?></span>
									<?php endif; ?>
								<?php endif; ?>
                                
				 <!--Product price End-->	
                
                 <!--Product Description-->
                  <div class="listingDescription">
				  	<?php 
                                        echo substr(strip_tags(wpsc_the_product_description()), 0, 50);
                                        echo '...';
                                        //shehryar
//					   $description = substr(wpsc_the_product_description(), 0, 50);
//				       echo substr($description, 0, strrpos($description, ' '))."...";
				    ?>
                    </div>
                 <!--Product Description End-->
                 	
                  <a href="<?php echo esc_url( wpsc_the_product_permalink() ); ?>" class="lm condensed sub1a smooth">Learn More</a>
                 
                   <!--Product Add to cart button-->
                 <form class="product_form"  enctype="multipart/form-data" action="<?php echo esc_url( $action ); ?>" method="post" name="product_<?php echo wpsc_the_product_id(); ?>" id="product_<?php echo wpsc_the_product_id(); ?>" >
                   <input type="hidden" value="add_to_cart" name="wpsc_ajax_action"/>
					<input type="hidden" value="<?php echo wpsc_the_product_id(); ?>" name="product_id"/>
                    <input type="submit" value="<?php _e('Add to bag', 'wpsc'); ?>" name="Buy" class="platbb condensed sub1a smooth" id="product_<?php echo wpsc_the_product_id(); ?>_submit_button"/>
                  </form>
                  <!--Product Add to cart button End-->
                  
                </div>
                <!--Product backend  End-->
              </div>
            </div>
       
 
       <?php endwhile; ?>
		<?php /** end the product loop here */?>
            
            
               </div>
          </div>
         