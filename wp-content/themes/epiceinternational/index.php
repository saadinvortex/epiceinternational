<?php
/**
 * Home Page
 *
 */

get_header(); ?>

<div id="whitebg">
  <div id="mainWrapper">
    <div id="contentMainWrapper">
      <div id="newtop" class="cat"> 
        
        <!-- bof upload alerts --> 
        <!-- eof upload alerts -->
        
        <div class="centerColumn" id="indexDefault">
          <div id="op" class="col-1-3 smooth">
            <div class="col-2-5"> <img src="<?php echo get_template_directory_uri(); ?>/images/skincare.png" alt="" /> </div>
            <div class="col-3-5">
              <h1><a href="<?php echo esc_url( home_url( '/' ) ); ?>products-page/skincare/">Our Products</a></h1>
              <a href="<?php echo esc_url( home_url( '/' ) ); ?>products-page/skincare/" class="condensed main">Explore our range</a> </div>
          </div>
          <div id="ads" class="col-1-3 smooth">
            <div class="col-2-5"> <img src="<?php echo get_template_directory_uri(); ?>/images/dr-spicer.png" alt="" /> </div>
            <div class="col-3-5">
              <h2><a href="<?php echo esc_url( home_url( '/' ) ); ?>ask-dr-spicer/">Ask Dr. Spicer</a></h2>
              <a href="<?php echo esc_url( home_url( '/' ) ); ?>ask-dr-spicer/" class="condensed main">Ask your questions</a> </div>
          </div>
          <div id="ae" class="col-1-3 smooth">
            <div class="col-2-5"> <img src="<?php echo get_template_directory_uri(); ?>/images/girl.png" alt="" /> </div>
            <div class="col-3-5">
              <h3><a href="<?php echo esc_url( home_url( '/' ) ); ?>about-epice/">About &Eacute;pic&eacute;</a></h3>
              <a href="<?php echo esc_url( home_url( '/' ) ); ?>about-epice/" class="condensed main">Learn more</a> </div>
          </div>
          <br class="clearBoth" />
          
          <!-- deprecated - to use uncomment this section
<div id="" class="content">This is the main define statement for the page for english when no template defined file exists. It is located in: <strong>/includes/languages/english/index.php</strong></div>
--> 
          
          <!-- deprecated - to use uncomment this section
<div id="" class="content">Define your main Index page copy here.</div>
-->
          
          <div id="indexDefaultMainContent">
            <h4 class="condensed">
            	Prevent <span data-icon="&#xe012;" aria-hidden="true"></span> 
            	Protect <span data-icon="&#xe012;" aria-hidden="true"></span>
           		Achieve <span data-icon="&#xe012;" aria-hidden="true"></span>
           		Maintain
            </h4>
            <div class="col-3-5"> 
           	<?php 
			$post = get_post(65); 
			$content = apply_filters('the_content', $post->post_content); 
			echo $content;  
			?>
            <a href="<?php echo esc_url( get_permalink( get_page_by_title( 'about epice' ) ) ); ?>" class="condensed blue1 head2 smooth">Learn more about &Eacute;pic&eacute;</a> 
            </div>
            <div class="col-2-5"> 
            	<?php echo get_the_post_thumbnail( '65' ); ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php get_footer(); ?>