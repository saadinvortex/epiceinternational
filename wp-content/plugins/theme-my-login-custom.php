<?php

function tml_registration_errors( $errors ) {
	if ( empty( $_POST['first_name'] ) )
		$errors->add( 'empty_first_name', '<strong>ERROR</strong>: Please enter your First Name.' );
	if ( empty( $_POST['last_name'] ) )
		$errors->add( 'empty_last_name', '<strong>ERROR</strong>: Please enter your Last Name.' );
	if ( empty( $_POST['phone'] ) )
		$errors->add( 'empty_phone', '<strong>ERROR</strong>: Please enter your Phone.' );
	if ( empty( $_POST['billing_address'] ) )
		$errors->add( 'empty_billing_address', '<strong>ERROR</strong>: Please enter your Billing Address.' );	
	if ( empty( $_POST['city'] ) )
		$errors->add( 'empty_city', '<strong>ERROR</strong>: Please enter your City.' );		
	if ( empty( $_POST['zip'] ) )
		$errors->add( 'empty_zip', '<strong>ERROR</strong>: Please enter your Zip.' );			
	return $errors;
}
add_filter( 'registration_errors', 'tml_registration_errors' );



function tml_user_register( $user_id ) {
	if ( !empty( $_POST['first_name'] ) )
		update_user_meta( $user_id, 'first_name', $_POST['first_name'] );
	if ( !empty( $_POST['last_name'] ) )
		update_user_meta( $user_id, 'last_name', $_POST['last_name'] );
	if ( !empty( $_POST['phone'] ) )
		update_user_meta( $user_id, 'phone', $_POST['phone'] );	
	if ( !empty( $_POST['billing_address'] ) )
		update_user_meta( $user_id, 'billing_address', $_POST['billing_address'] );	
	if ( !empty( $_POST['city'] ) )
		update_user_meta( $user_id, 'city', $_POST['city'] );
		if (!empty( $_POST['zip'] ) )
		update_user_meta( $user_id, 'zip', $_POST['zip'] );	
}
add_action( 'user_register', 'tml_user_register' );




?>