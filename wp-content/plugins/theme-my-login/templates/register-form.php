<?php
/*
If you would like to edit this file, copy it to your current theme's directory and edit it there.
Theme My Login will always look in your theme's directory first, before using this default template.
*/
?>
<?php $template->the_action_template_message( 'register' ); ?>
<?php $template->the_errors(); ?>

<form name="registerform" id="registerform<?php $template->the_instance(); ?>" action="<?php $template->the_action_url( 'register' ); ?>" method="post">
  <fieldset id="register">
    <legend class="condensed">Register</legend>
    <fieldset class="col-1-2">
      <legend>Personal details</legend>
      <label for="first_name<?php $template->the_instance(); ?>" class="inputLabel">
        <?php _e( 'First name', 'theme-my-login' ) ?>
      </label>
      <input type="text" required=""  name="first_name" id="first_name<?php $template->the_instance(); ?>" class="input" value="<?php $template->the_posted_value( 'first_name' ); ?>" size="20" tabindex="20" />
      <label for="last_name<?php $template->the_instance(); ?>" class="inputLabel">
        <?php _e( 'Last name', 'theme-my-login' ) ?>
      </label>
      <input type="text" required=""  name="last_name" id="last_name<?php $template->the_instance(); ?>" class="input" value="<?php $template->the_posted_value( 'last_name' ); ?>" size="20" tabindex="20" />
      <label for="phone<?php $template->the_instance(); ?>" class="inputLabel">
        <?php _e( 'Phone', 'theme-my-login' ) ?>
      </label>
      <input type="text"  required=""  name="phone" id="phone<?php $template->the_instance(); ?>" class="input" value="<?php $template->the_posted_value( 'phone' ); ?>" size="20" tabindex="20" />
      <label for="billing_address<?php $template->the_instance(); ?>" class="inputLabel">
        <?php _e( 'Billing address', 'theme-my-login' ) ?>
      </label>
      <input type="text" required=""  name="billing_address" id="billing_address<?php $template->the_instance(); ?>" class="input" value="<?php $template->the_posted_value( 'billing_address' ); ?>" size="20" tabindex="20" />
      <label for="city<?php $template->the_instance(); ?>" class="inputLabel">
        <?php _e( 'City', 'theme-my-login' ) ?>
      </label>
      <input type="text" required=""  name="city" id="city<?php $template->the_instance(); ?>" class="input" value="<?php $template->the_posted_value( 'city' ); ?>" size="20" tabindex="20" />
      <div class="col-1-2 state">
        <label id="zoneLabel" for="stateZone" class="inputLabel">
          <?php _e( 'State/Province', 'theme-my-login' ) ?>
        </label>
        <label class="wrapselect forstate">
          <select id="stateZone" name="zone_id">
            <option value=""></option>
            <option value="Alabama">Alabama</option>
            <option value="Alaska">Alaska</option>
            <option value="American Samoa">American Samoa</option>
            <option value="Arizona">Arizona</option>
            <option value="Arkansas">Arkansas</option>
            <option value="Armed Forces Americas">Armed Forces Americas</option>
            <option value="Armed Forces Europe">Armed Forces Europe</option>
            <option value="Armed Forces Pacific">Armed Forces Pacific</option>
            <option value="California">California</option>
            <option value="Colorado">Colorado</option>
            <option value="Connecticut">Connecticut</option>
            <option value="Delaware">Delaware</option>
            <option value="District of Columbia">District of Columbia</option>
            <option value="Federated States Of Micronesia">Federated States Of Micronesia</option>
            <option value="Florida">Florida</option>
            <option value="Georgia">Georgia</option>
            <option value="Guam">Guam</option>
            <option value="Hawaii">Hawaii</option>
            <option value="Idaho">Idaho</option>
            <option value="Illinois">Illinois</option>
            <option value="Indiana">Indiana</option>
            <option value="Iowa">Iowa</option>
            <option value="Kansas">Kansas</option>
            <option value="Kentucky">Kentucky</option>
            <option value="Louisiana">Louisiana</option>
            <option value="Maine">Maine</option>
            <option value="Marshall Islands">Marshall Islands</option>
            <option value="Maryland">Maryland</option>
            <option value="Massachusetts">Massachusetts</option>
            <option value="Michigan">Michigan</option>
            <option value="Minnesota">Minnesota</option>
            <option value="Mississippi">Mississippi</option>
            <option value="Missouri">Missouri</option>
            <option value="Montana">Montana</option>
            <option value="Nebraska">Nebraska</option>
            <option value="Nevada">Nevada</option>
            <option value="New Hampshire">New Hampshire</option>
            <option value="New Jersey">New Jersey</option>
            <option value="New Mexico">New Mexico</option>
            <option value="New York">New York</option>
            <option value="North Carolina">North Carolina</option>
            <option value="North Dakota">North Dakota</option>
            <option value="Northern Mariana Islands">Northern Mariana Islands</option>
            <option value="Ohio">Ohio</option>
            <option value="Oklahoma">Oklahoma</option>
            <option value="Oregon">Oregon</option>
            <option value="Pennsylvania">Pennsylvania</option>
            <option value="Puerto Rico">Puerto Rico</option>
            <option value="Rhode Island">Rhode Island</option>
            <option value="South Carolina">South Carolina</option>
            <option value="South Dakota">South Dakota</option>
            <option value="Tennessee">Tennessee</option>
            <option value="Texas">Texas</option>
            <option value="Utah">Utah</option>
            <option value="Vermont">Vermont</option>
            <option value="Virgin Islands">Virgin Islands</option>
            <option value="Virginia">Virginia</option>
            <option value="Washington">Washington</option>
            <option value="West Virginia">West Virginia</option>
            <option value="Wisconsin">Wisconsin</option>
            <option value="Wyoming">Wyoming</option>
          </select>
        </label>
        <br class="clearBoth">
      </div>
      <div class="col-1-2 zip">
        <label for="zip<?php $template->the_instance(); ?>" required=""  class="inputLabel">
          <?php _e( 'Zip/Post Code', 'theme-my-login' ) ?>
        </label>
        <input type="text" name="zip" id="zip<?php $template->the_instance(); ?>" required=""  class="input" value="<?php $template->the_posted_value( 'zip' ); ?>" size="11"  maxlength="10"/>
      </div>
      <br class="clearBoth">
      <label id="stateLabel" for="state" class="hiddenField" classname="hiddenField">Enter State</label>
      <input type="text" id="state" maxlength="32" size="33" value="&nbsp;" name="state" disabled="" required=""  class="hiddenField" classname="hiddenField">
      <span id="stText" class="hiddenField" classname="hiddenField">*</span>
      <label for="country" class="inputLabel">Country</label>
      <label class="wrapselect">
        <select onchange="update_zone(this.form);" id="country" name="zone_country_id" class="valid">
          <option value="">Please Choose Your Country</option>
          <option selected="selected" value="223">United States</option>
          <option value="240">Aaland Islands</option>
          <option value="1">Afghanistan</option>
          <option value="2">Albania</option>
          <option value="3">Algeria</option>
          <option value="4">American Samoa</option>
          <option value="5">Andorra</option>
          <option value="6">Angola</option>
          <option value="7">Anguilla</option>
          <option value="8">Antarctica</option>
          <option value="9">Antigua and Barbuda</option>
          <option value="10">Argentina</option>
          <option value="11">Armenia</option>
          <option value="12">Aruba</option>
          <option value="13">Australia</option>
          <option value="14">Austria</option>
          <option value="15">Azerbaijan</option>
          <option value="16">Bahamas</option>
          <option value="17">Bahrain</option>
          <option value="18">Bangladesh</option>
          <option value="19">Barbados</option>
          <option value="20">Belarus</option>
          <option value="21">Belgium</option>
          <option value="22">Belize</option>
          <option value="23">Benin</option>
          <option value="24">Bermuda</option>
          <option value="25">Bhutan</option>
          <option value="26">Bolivia</option>
          <option value="27">Bosnia and Herzegowina</option>
          <option value="28">Botswana</option>
          <option value="29">Bouvet Island</option>
          <option value="30">Brazil</option>
          <option value="31">British Indian Ocean Territory</option>
          <option value="32">Brunei Darussalam</option>
          <option value="33">Bulgaria</option>
          <option value="34">Burkina Faso</option>
          <option value="35">Burundi</option>
          <option value="36">Cambodia</option>
          <option value="37">Cameroon</option>
          <option value="38">Canada</option>
          <option value="39">Cape Verde</option>
          <option value="40">Cayman Islands</option>
          <option value="41">Central African Republic</option>
          <option value="42">Chad</option>
          <option value="43">Chile</option>
          <option value="44">China</option>
          <option value="45">Christmas Island</option>
          <option value="46">Cocos (Keeling) Islands</option>
          <option value="47">Colombia</option>
          <option value="48">Comoros</option>
          <option value="49">Congo</option>
          <option value="50">Cook Islands</option>
          <option value="51">Costa Rica</option>
          <option value="52">Cote D'Ivoire</option>
          <option value="53">Croatia</option>
          <option value="54">Cuba</option>
          <option value="55">Cyprus</option>
          <option value="56">Czech Republic</option>
          <option value="57">Denmark</option>
          <option value="58">Djibouti</option>
          <option value="59">Dominica</option>
          <option value="60">Dominican Republic</option>
          <option value="62">Ecuador</option>
          <option value="63">Egypt</option>
          <option value="64">El Salvador</option>
          <option value="65">Equatorial Guinea</option>
          <option value="66">Eritrea</option>
          <option value="67">Estonia</option>
          <option value="68">Ethiopia</option>
          <option value="69">Falkland Islands (Malvinas)</option>
          <option value="70">Faroe Islands</option>
          <option value="71">Fiji</option>
          <option value="72">Finland</option>
          <option value="73">France</option>
          <option value="75">French Guiana</option>
          <option value="76">French Polynesia</option>
          <option value="77">French Southern Territories</option>
          <option value="78">Gabon</option>
          <option value="79">Gambia</option>
          <option value="80">Georgia</option>
          <option value="81">Germany</option>
          <option value="82">Ghana</option>
          <option value="83">Gibraltar</option>
          <option value="84">Greece</option>
          <option value="85">Greenland</option>
          <option value="86">Grenada</option>
          <option value="87">Guadeloupe</option>
          <option value="88">Guam</option>
          <option value="89">Guatemala</option>
          <option value="243">Guernsey</option>
          <option value="90">Guinea</option>
          <option value="91">Guinea-bissau</option>
          <option value="92">Guyana</option>
          <option value="93">Haiti</option>
          <option value="94">Heard and Mc Donald Islands</option>
          <option value="95">Honduras</option>
          <option value="96">Hong Kong</option>
          <option value="97">Hungary</option>
          <option value="98">Iceland</option>
          <option value="99">India</option>
          <option value="100">Indonesia</option>
          <option value="101">Iran (Islamic Republic of)</option>
          <option value="102">Iraq</option>
          <option value="103">Ireland</option>
          <option value="244">Isle of Man</option>
          <option value="104">Israel</option>
          <option value="105">Italy</option>
          <option value="106">Jamaica</option>
          <option value="107">Japan</option>
          <option value="245">Jersey</option>
          <option value="108">Jordan</option>
          <option value="109">Kazakhstan</option>
          <option value="110">Kenya</option>
          <option value="111">Kiribati</option>
          <option value="112">Korea, Democratic People's Republic of</option>
          <option value="113">Korea, Republic of</option>
          <option value="114">Kuwait</option>
          <option value="115">Kyrgyzstan</option>
          <option value="116">Lao People's Democratic Republic</option>
          <option value="117">Latvia</option>
          <option value="118">Lebanon</option>
          <option value="119">Lesotho</option>
          <option value="120">Liberia</option>
          <option value="121">Libyan Arab Jamahiriya</option>
          <option value="122">Liechtenstein</option>
          <option value="123">Lithuania</option>
          <option value="124">Luxembourg</option>
          <option value="125">Macao</option>
          <option value="126">Macedonia, The Former Yugoslav Republic of</option>
          <option value="127">Madagascar</option>
          <option value="128">Malawi</option>
          <option value="129">Malaysia</option>
          <option value="130">Maldives</option>
          <option value="131">Mali</option>
          <option value="132">Malta</option>
          <option value="133">Marshall Islands</option>
          <option value="134">Martinique</option>
          <option value="135">Mauritania</option>
          <option value="136">Mauritius</option>
          <option value="137">Mayotte</option>
          <option value="138">Mexico</option>
          <option value="139">Micronesia, Federated States of</option>
          <option value="140">Moldova</option>
          <option value="141">Monaco</option>
          <option value="142">Mongolia</option>
          <option value="242">Montenegro</option>
          <option value="143">Montserrat</option>
          <option value="144">Morocco</option>
          <option value="145">Mozambique</option>
          <option value="146">Myanmar</option>
          <option value="147">Namibia</option>
          <option value="148">Nauru</option>
          <option value="149">Nepal</option>
          <option value="150">Netherlands</option>
          <option value="151">Netherlands Antilles</option>
          <option value="152">New Caledonia</option>
          <option value="153">New Zealand</option>
          <option value="154">Nicaragua</option>
          <option value="155">Niger</option>
          <option value="156">Nigeria</option>
          <option value="157">Niue</option>
          <option value="158">Norfolk Island</option>
          <option value="159">Northern Mariana Islands</option>
          <option value="160">Norway</option>
          <option value="161">Oman</option>
          <option value="162">Pakistan</option>
          <option value="163">Palau</option>
          <option value="241">Palestinian Territory</option>
          <option value="164">Panama</option>
          <option value="165">Papua New Guinea</option>
          <option value="166">Paraguay</option>
          <option value="167">Peru</option>
          <option value="168">Philippines</option>
          <option value="169">Pitcairn</option>
          <option value="170">Poland</option>
          <option value="171">Portugal</option>
          <option value="172">Puerto Rico</option>
          <option value="173">Qatar</option>
          <option value="174">Reunion</option>
          <option value="175">Romania</option>
          <option value="176">Russian Federation</option>
          <option value="177">Rwanda</option>
          <option value="178">Saint Kitts and Nevis</option>
          <option value="179">Saint Lucia</option>
          <option value="180">Saint Vincent and the Grenadines</option>
          <option value="181">Samoa</option>
          <option value="182">San Marino</option>
          <option value="183">Sao Tome and Principe</option>
          <option value="184">Saudi Arabia</option>
          <option value="185">Senegal</option>
          <option value="236">Serbia</option>
          <option value="186">Seychelles</option>
          <option value="187">Sierra Leone</option>
          <option value="188">Singapore</option>
          <option value="189">Slovakia (Slovak Republic)</option>
          <option value="190">Slovenia</option>
          <option value="191">Solomon Islands</option>
          <option value="192">Somalia</option>
          <option value="193">South Africa</option>
          <option value="194">South Georgia and the South Sandwich Islands</option>
          <option value="195">Spain</option>
          <option value="196">Sri Lanka</option>
          <option value="197">St. Helena</option>
          <option value="198">St. Pierre and Miquelon</option>
          <option value="199">Sudan</option>
          <option value="200">Suriname</option>
          <option value="201">Svalbard and Jan Mayen Islands</option>
          <option value="202">Swaziland</option>
          <option value="203">Sweden</option>
          <option value="204">Switzerland</option>
          <option value="205">Syrian Arab Republic</option>
          <option value="206">Taiwan</option>
          <option value="207">Tajikistan</option>
          <option value="208">Tanzania, United Republic of</option>
          <option value="209">Thailand</option>
          <option value="61">Timor-Leste</option>
          <option value="210">Togo</option>
          <option value="211">Tokelau</option>
          <option value="212">Tonga</option>
          <option value="213">Trinidad and Tobago</option>
          <option value="214">Tunisia</option>
          <option value="215">Turkey</option>
          <option value="216">Turkmenistan</option>
          <option value="217">Turks and Caicos Islands</option>
          <option value="218">Tuvalu</option>
          <option value="219">Uganda</option>
          <option value="220">Ukraine</option>
          <option value="221">United Arab Emirates</option>
          <option value="222">United Kingdom</option>
          <option value="224">United States Minor Outlying Islands</option>
          <option value="225">Uruguay</option>
          <option value="226">Uzbekistan</option>
          <option value="227">Vanuatu</option>
          <option value="228">Vatican City State (Holy See)</option>
          <option value="229">Venezuela</option>
          <option value="230">Viet Nam</option>
          <option value="231">Virgin Islands (British)</option>
          <option value="232">Virgin Islands (U.S.)</option>
          <option value="233">Wallis and Futuna Islands</option>
          <option value="234">Western Sahara</option>
          <option value="235">Yemen</option>
          <option value="238">Zambia</option>
          <option value="239">Zimbabwe</option>
        </select>
      </label>
      <br class="clearBoth">
    </fieldset>
    <fieldset class="col-1-2 rrr">
      <legend>Account details</legend>
      <label for="user_login<?php $template->the_instance(); ?>" class="inputLabel">
        <?php _e( 'Username' ); ?>
      </label>
      <input type="text" required="" name="user_login" id="user_login<?php $template->the_instance(); ?>" class="input" value="<?php $template->the_posted_value( 'user_login' ); ?>" size="20" />
      <label  class="inputLabel" for="user_email<?php $template->the_instance(); ?>">
        <?php _e( 'E-mail' ); ?>
      </label>
      <input type="text" required="" name="user_email" id="user_email<?php $template->the_instance(); ?>" class="input" value="<?php $template->the_posted_value( 'user_email' ); ?>" size="20" />
      <?php do_action( 'register_form' ); ?>
      <p class="inputLabel" id="reg_passmail<?php $template->the_instance(); ?>"><?php echo apply_filters( 'tml_register_passmail_template_message', __( 'A password will be e-mailed to you.' ) ); ?></p>
      <br class="clearBoth">
      <div class="buttonRow back">
        <input type="submit" class="pseudo" name="wp-submit" id="wp-submit<?php $template->the_instance(); ?>" value="<?php esc_attr_e( 'Register' ); ?>" />
        <input type="hidden" name="redirect_to" value="<?php $template->the_redirect_url( 'register' ); ?>" />
        <input type="hidden" name="instance" value="<?php $template->the_instance(); ?>" />
        <input type="hidden" name="action" value="register" />
      </div>
    </fieldset>
  </fieldset>
</form>
<?php // $template->the_action_links( array( 'register' => false ) ); ?>
