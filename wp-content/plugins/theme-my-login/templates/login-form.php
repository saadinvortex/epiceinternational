<?php
/*
If you would like to edit this file, copy it to your current theme's directory and edit it there.
Theme My Login will always look in your theme's directory first, before using this default template.
*/
?>
<div id="loginDefault" class="centerColumn">
	<?php $template->the_action_template_message( 'login' ); ?>
	<?php $template->the_errors(); ?>
	<form name="loginform" id="loginform<?php $template->the_instance(); ?>" action="<?php $template->the_action_url( 'login' ); ?>" method="post">
    
    <fieldset id="existing">
		<legend class="condensed">Login to your Account</legend>
        <div class="forward s">
			<a href="<?php echo esc_url( home_url( '/' ) ); ?>login/?action=lostpassword">Forgotten your password?</a> No problem, recover it now	
        </div>
        <br class="clearBoth">
		<div class="col-45">
			<input type="text" id="login-email-address" placeholder="Enter your e-mail address"  name="log" maxlength="96" size="41" class="input" value="<?php $template->the_posted_value( 'log' ); ?>" />
		</div>
		<div class="col-45">
			<input type="password" name="pwd" placeholder="Enter your password"  id="login-password" class="input" value=""  maxlength="40" size="41" />
		</div>

		<?php do_action( 'login_form' ); ?>

			<div class="buttonRow forward col-1-10">
			<input type="submit" name="wp-submit" class="pseudo" id="wp-submit<?php $template->the_instance(); ?>" value="<?php esc_attr_e( 'Log In' ); ?>" />
			<input type="hidden" name="redirect_to" value="<?php $template->the_redirect_url( 'login' ); ?>" />
			<input type="hidden" name="instance" value="<?php $template->the_instance(); ?>" />
			<input type="hidden" name="action" value="login" />
		</div>
	</fieldset>

    </form>
	<?php // $template->the_action_links( array( 'login' => false ) ); ?>

