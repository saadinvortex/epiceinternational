<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'epiceinternational');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '5U8IZm:*U^@@V]Q]},+RJ^Oog{B|u`4c|1(LIjaE}M>X]_}pGOKG]6478+12Tc0L');
define('SECURE_AUTH_KEY',  '!A_O5UlwwfEJ}zJt!xL_9/*M5-AVb9%#WMpWM5?XIJcg9x zP=5W#OPMe,E[|7*O');
define('LOGGED_IN_KEY',    'xj9ER .k15hY(J;qE6#KyCR5+(Q(/cf>==!y7qCEX[ug`,[5xG7+Ig7G~A3dDnYV');
define('NONCE_KEY',        'l8A*X7TB,G.[-~c ilA,C.bsJL]XHn3]^!DADN?38T_.~`M_7TDl;8#@jZ>[m:aS');
define('AUTH_SALT',        'nw~Kcyyxc_xk[k_2ugW/2^BVwC`[!Qg>9d37X %1#e?/C-ULYu~`3Y7L:Judcl]y');
define('SECURE_AUTH_SALT', 'b)2yZ0UH@]<e!.E1Snjw)@*=fl99st%Fk/PAlGn!gzck{*QVM@-+{e_z*]-k*uEK');
define('LOGGED_IN_SALT',   'O~q6@b1-EB7;P+=o~6C<i}ofMFBwZx 9c!nA6x|o pJ4cC)/a_tC$=j^C.WFBR{$');
define('NONCE_SALT',       '2H+UXz2&w09V/.pFY[<Ga62$(kC-R@P^{g~Q$,o]BpnCPD62i_M*h{IGV|a/ehG}');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'epice_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
